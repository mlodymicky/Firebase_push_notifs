const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

exports.sendNotifications = functions.database.ref('/notifications/{notificationId}').onWrite((event) => {
    // Exit if data already created
    if (event.before.exists()) {
        return;
    }

    // Exit when the data is deleted
    if (!event.after.exists()) {
        return;
    }

    // Setup notification
    const NOTIFICATION_SNAPSHOT = event.after.val();
    const payload = {
        notification: {
            title: `${NOTIFICATION_SNAPSHOT.user} napisał!`,
            body: NOTIFICATION_SNAPSHOT.message,
            icon: NOTIFICATION_SNAPSHOT.userProfileImg
        }
    };
    console.info(payload)
    function cleanInvalidTokens(tokensWithKey, results) {
        const invalidTokens = [];
        results.forEach((result, i) => {
            if (!result.error) return;
            console.error("Error with token: ", tokensWithKey[i].token)
            switch (result.error.code) {
                case "messaging/invalid-registration-token":
                case "messaging/registration-token-not-registered":
                    invalidTokens.push(admin.database().ref("/tokens").child(tokensWithKey[i].key).remove())
                    break;
                default:
                    break;
            }
        });
        return Promise.all(invalidTokens)
    }
    return admin.database().ref('/tokens').once('value').then((data) => {
        if (!data.val()) return;

        const snapshot = data.val();
        const tokens = [];
        const tokensWithKey = [];
        for (let key in snapshot) {
            tokens.push(snapshot[key].token);
            tokensWithKey.push({
                token: snapshot[key].token,
                key: key
            });
        }
        return admin.messaging().sendToDevice(tokens, payload)
            .then((response) => cleanInvalidTokens(tokensWithKey, response.results))
            .then(() => admin.database().ref("/notifications").child(event.after.key).remove())
    });
});