{

  /* ========================
    Variables
  ======================== */

  const FIREBASE_AUTH = firebase.auth();
  const FIREBASE_MESSAGING = firebase.messaging();
  const FIREBASE_DATABASE = firebase.database();

  const signInButton = document.getElementById('sign-in');
  const signOutButton = document.getElementById('sign-out');
  const subscribe = document.getElementById('subscribe');
  const unSubscribe = document.getElementById('unsubscribe');
  const sendForm = document.getElementById('send-form');
  /* ========================
    Event Listeners
  ======================== */

  signInButton.addEventListener("click", signIn);
  signOutButton.addEventListener("click", signOut);
  subscribe.addEventListener("click", subscribeToNotifications);
  unSubscribe.addEventListener("click", unsubscribeFromNotifications);
  sendForm.addEventListener("submit", sendNotification);

  FIREBASE_AUTH.onAuthStateChanged(handleAuthStateChanged);
  FIREBASE_MESSAGING.onTokenRefresh(handleTokenRefresh);


  /* ========================
    Functions
  ======================== */

  function signIn() {
    FIREBASE_AUTH.signInWithPopup(new firebase.auth.GoogleAuthProvider())
  }

  function signOut() {
    FIREBASE_AUTH.signOut()
  }

  function handleAuthStateChanged(user) {
    if (user) {
      console.log(user)
      signInButton.setAttribute("hidden", "true")
      signOutButton.removeAttribute("hidden")
      checkSubscription()

    }
    else {
      console.log("no-user");
      signInButton.removeAttribute("hidden")
      signOutButton.setAttribute("hidden", "true")
    }
  }
  function subscribeToNotifications() {
    FIREBASE_MESSAGING.requestPermission()
      .then(() => handleTokenRefresh())
      .then(() => checkSubscription())
      .catch((err) => console.log("Error ", err))
  }
  function handleTokenRefresh() {
    return FIREBASE_MESSAGING.getToken()
      .then((token) => {
        FIREBASE_DATABASE.ref('/tokens').push({
          token: token,
          uid: FIREBASE_AUTH.currentUser.uid
        })
      })
  }
  function unsubscribeFromNotifications() {
    FIREBASE_MESSAGING.getToken()
      .then((token) => FIREBASE_MESSAGING.deleteToken(token))
      .then(() => FIREBASE_DATABASE.ref('/tokens').orderByChild('uid').equalTo(FIREBASE_AUTH.currentUser.uid)
        .once('value'))
      .then((snapshot) => {
        console.log(snapshot.val())
        const key = Object.keys(snapshot.val())[0];
        return FIREBASE_DATABASE.ref('/tokens').child(key).remove();
      })
      .then(() => checkSubscription())
      .catch(() => console.log("unsubscription failed"))
  }

  function checkSubscription() {
    FIREBASE_DATABASE.ref('/tokens').orderByChild('uid').equalTo(FIREBASE_AUTH.currentUser.uid)
      .once('value')
      .then((snapshot) => {
        if (snapshot.val()) {
          unSubscribe.removeAttribute("hidden")
          subscribe.setAttribute("hidden", "true")
        } else {
          subscribe.removeAttribute("hidden")
          unSubscribe.setAttribute("hidden", "true")
        }
      })
  }

  function sendNotification(e) {
    e.preventDefault();

    const notificationMessage = document.getElementById('message').value;
    if (!notificationMessage) return;

    FIREBASE_DATABASE.ref('/notifications')
      .push({
        user: FIREBASE_AUTH.currentUser.displayName,
        message: notificationMessage,
        userProfileImg: FIREBASE_AUTH.currentUser.photoURL
      })
      .then(() => {
        document.getElementById('message').value = "";
      })
      .catch(() => {
        console.log("error sending notification :(")
      });
  }
}

